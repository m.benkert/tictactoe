from Game import Game
from PlayerMock import PlayerMock
from unittest import TestCase


def create_game():
    player_a = PlayerMock("Me")
    player_b = PlayerMock("You")
    game = Game(player_a, player_b)
    return game


class GameTest(TestCase):

    def test_init(self):
        # given
        player_a = PlayerMock("Me")
        player_b = PlayerMock("You")
        # when
        game = Game(player_a, player_b)
        # then
        self.assertIsNotNone(game.game)
        self.assertIsNotNone(game.colors)
        self.assertIsNotNone(game.players)
        self.assertIsNone(game.current)

    def test_new_game(self):
        # given
        game = create_game()
        # when
        game.new_game()

    def test_active_player(self):
        # given
        player_a = PlayerMock("Me")
        player_b = PlayerMock("You")
        game = Game(player_a, player_b)
        game.current = 1
        # when
        active_player = game.active_player()
        # then
        self.assertEqual(player_a, active_player)
        self.assertNotEqual(player_b, active_player)

    def test_game_is_over_yes_winner(self):
        # given
        game = create_game()
        game.game.fields = [1, 1, 1, 2, 0, 2, 0, 0, 0]
        # when
        is_over = game.game_is_over()
        # then
        self.assertTrue(is_over)

    def test_game_is_over_yes_tied(self):
        # given
        game = create_game()
        game.game.fields = [1, 2, 1, 2, 1, 2, 2, 1, 2]
        # when
        is_over = game.game_is_over()
        # then
        self.assertTrue(is_over)

    def test_game_is_over_no(self):
        # given
        game = create_game()
        game.game.fields = [0, 0, 0, 1, 1, 2, 2, 1, 2]
        # when
        is_over = game.game_is_over()
        # then
        self.assertFalse(is_over)

    def test_someone_has_won_yes(self):
        # given
        game = create_game()
        game.game.fields = [1, 1, 1, 2, 0, 2, 0, 0, 0]
        # when
        winner = game.someone_has_won()
        # then
        self.assertEqual(1, winner)

    def test_someone_has_won_no(self):
        # given
        game = create_game()
        game.game.fields = [0, 0, 0, 1, 1, 2, 2, 1, 2]
        # when
        winner = game.someone_has_won()
        # then
        self.assertEqual(0, winner)

    def test_visualize(self):
        # given
        game = create_game()
        # when
        game.visualize()
