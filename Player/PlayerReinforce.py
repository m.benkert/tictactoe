import copy
import json
import time
import random

from .Base import Player


class Reinforce(Player):

    def __init__(self, name="Reinforcement Learning", thinking_time=0.5, load_from_file=None,
                 gamma=0.8, alpha_min=0.1, alpha_max=0.8, epsilon_min=0.1, epsilon_max=1.0, episodes=10000):
        super().__init__(name)
        self.thinking_time = thinking_time

        # discount factor
        self.gamma = gamma
        # duration of learning
        self.episodes = episodes
        self.current_episode = -1
        # learning rate
        self.alpha_min = alpha_min
        self.alpha_max = alpha_max
        self.alpha_ratio = 1.0
        # threshold
        self.epsilon_min = epsilon_min
        self.epsilon_max = epsilon_max
        self.epsilon_ratio = 1.0

        # q-learning
        self.Q = {}
        if load_from_file is None:
            self.init_q()
        else:
            self.load_object_from_file(load_from_file)

        # memory
        self.current_state = None
        self.current_action = None

    def new_training(self, episodes=None):
        self.current_episode = -1
        if episodes is not None:
            self.episodes = episodes

    def save_object_to_file(self, filename="Player/reinforce.json"):
        obj = {"Q": json.dumps(list(self.Q.items()))}

        with open(filename, "w") as output:
            json.dump(obj, output)

    def load_object_from_file(self, filename="Player/reinforce.json"):
        with open(filename, "r") as output:
            obj = json.load(output)

        q_table = json.loads(obj["Q"])
        for key, value in q_table:
            self.Q[tuple(key)] = value

    def alpha(self):
        return round(max(0.0, (((self.alpha_max - self.alpha_min) / (self.episodes / self.alpha_ratio))
                               * ((self.episodes / self.alpha_ratio) - self.current_episode))) + self.alpha_min, 4)

    def epsilon(self):
        return round(self.epsilon_max - max(0.0, (((self.epsilon_max - self.epsilon_min)
                                                   / (self.episodes / self.epsilon_ratio))
                                                  * ((self.episodes / self.epsilon_ratio) - self.current_episode))), 4)

    def init_q(self):
        for x_0 in range(3):
            for x_1 in range(3):
                for x_2 in range(3):
                    for x_3 in range(3):
                        for x_4 in range(3):
                            for x_5 in range(3):
                                for x_6 in range(3):
                                    for x_7 in range(3):
                                        for x_8 in range(3):
                                            q_val = [0, 0, 0, 0, 0, 0, 0, 0, 0]
                                            self.Q[x_0, x_1, x_2, x_3, x_4, x_5, x_6, x_7, x_8] = q_val

    def on_game_start(self):
        self.current_state = None
        self.current_action = None
        self.current_episode += 1

    def on_turn(self, board):
        time.sleep(self.thinking_time)

        if self.number == 2:
            board.swap_numbers()

        fields = board.get_fields()

        if self.current_state is not None:
            if self.current_state == fields:
                self.reinforcement([2, 2, 2, 2, 2, 2, 2, 2, 2], -20)
            else:
                self.reinforcement(fields, 1)

        self.current_state = fields
        self.current_action = self.choose_action()
        return self.current_action

    def choose_action(self, print_flag=False):
        random_val = random.uniform(0, 1)
        epsilon = self.epsilon()
        if print_flag:
            print("Random Val: {} Epsilon: {}".format(random_val, epsilon))
        if random_val >= epsilon:
            next_action = random.randrange(9)
            selection_style = 'Random'
        else:
            next_action = self.get_max_q_action()
            selection_style = 'Max Q'
        if print_flag:
            print("{} {}".format(selection_style, next_action))
        return next_action

    def get_max_q_action(self):
        actions = []
        q_values = self.Q.get(tuple(self.current_state))

        max_value = max(q_values)
        for index, value in enumerate(q_values):
            if value == max_value:
                actions.append(index)
        return random.choice(actions)

    def reinforcement(self, next_state, reward, print_flag=False):
        alpha = self.alpha()
        max_q = max(self.Q.get(tuple(next_state)))
        q_neu = (1.0 - alpha) * self.Q.get(tuple(self.current_state))[
            self.current_action] + alpha * (reward + self.gamma * max_q)

        old_q_values = copy.deepcopy(self.Q.get(tuple(self.current_state)))

        self.Q[tuple(self.current_state)][self.current_action] = round(q_neu, 4)

        new_q_values = copy.deepcopy(self.Q.get(tuple(self.current_state)))

        if print_flag:
            print("Fields: {} Action: {} Old Q: {} New Q: {}".format(self.current_state, self.current_action,
                                                                     old_q_values[self.current_action],
                                                                     new_q_values[self.current_action]))

    def on_game_end(self, winner):
        if winner == self.number:
            self.reinforcement([1, 1, 1, 1, 1, 1, 1, 1, 1], 5)
        elif winner == 0:
            self.reinforcement([1, 1, 1, 1, 1, 1, 1, 1, 1], 2)
        else:
            self.reinforcement([2, 2, 2, 2, 2, 2, 2, 2, 2], -10)
