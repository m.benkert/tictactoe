from .Base import Player
from Interface.Base import Interface


class Human(Player):

    def __init__(self, name="Human", interface: Interface = None):
        super().__init__(name)
        self.interface = interface

    def on_turn(self, board):
        return self.interface.interactive_turn(board, self.number)
