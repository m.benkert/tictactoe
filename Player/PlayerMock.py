from .Base import Player


class Mock(Player):

    def __init__(self, name="Mock"):
        super().__init__(name)

    def on_turn(self, board):
        for index, element in enumerate(board.get_fields()):
            if element == 0:
                return index
