from .Base import Player
from unittest import TestCase


class PlayerTest(TestCase):

    def test_init(self):
        # given
        name = "Maximilian"
        # when
        player = Player(name)
        # then
        self.assertEqual(name, player.name)
        self.assertEqual(0, player.number)

    def test_set_number(self):
        # given
        player = Player("Maximilian")
        # when
        player.set_number(1)
        # then
        self.assertEqual(1, player.number)

    def test_get_name(self):
        # given
        given_name = "Maximilian"
        player = Player(given_name)
        # when
        name = player.get_name()
        # then
        self.assertEqual(given_name, name)
