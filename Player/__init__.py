from .PlayerANN import ANN
from .PlayerHuman import Human
from .PlayerMock import Mock
from .PlayerRandom import Random
from .PlayerReinforce import Reinforce
from .PlayerValidation import Validation
