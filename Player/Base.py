from Board import Board


class Player:

    def __init__(self, name: str):
        self.name = name
        self.number = 0

    def set_number(self, number: int):
        self.number = number

    def on_game_start(self):
        pass

    def on_turn(self, board: Board):
        pass

    def on_game_end(self, winner: int):
        pass

    def get_name(self):
        return self.name
