import time

import numpy as np
import pandas as pd
from keras.models import load_model

from .Base import Player


class ANN(Player):

    def __init__(self, name="Artificial Neural Network", thinking_time=0.5):
        super().__init__(name)
        self.thinking_time = thinking_time
        self.ann = load_model("tictactoe.h5")

    def on_game_start(self):
        pass

    def on_turn(self, board):
        time.sleep(self.thinking_time)

        if self.number == 2:
            board.swap_numbers()

        fields = board.get_fields()
        data = [[0] * 9, [1] * 9, [2] * 9, fields]

        columns = ["field" + str(x) for x in range(9)]
        sample = pd.DataFrame(np.array(data).reshape(4, 9), columns=columns)

        sample["turn"] = (sample[columns] != 0).sum(axis=1)
        sample = pd.get_dummies(sample, columns=columns, drop_first=True)
        sample = sample.drop([0, 1, 2])

        sample["salt"] = np.random.randint(0, 8, 1)

        best_index = 0
        best_value = -np.inf
        for index, field in enumerate(fields):
            if fields[index] == 0:
                sample_ = sample.copy()
                sample_["field" + str(index) + "_1"] = 1

                value = self.ann.predict(sample_)

                if value > best_value:
                    best_index = index
                    best_value = value

        return best_index

    def on_game_end(self, winner):
        pass
