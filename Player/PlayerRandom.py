import random
import time

from Board import Board
from .Base import Player


class Random(Player):

    def __init__(self, name="Random", thinking_time=0.5):
        super().__init__(name)
        self.thinking_time = thinking_time

    def on_turn(self, board):
        time.sleep(self.thinking_time)

        empty_fields = []
        for index, element in enumerate(board.get_fields()):
            if element == 0:
                empty_fields.append(index)

        field = empty_fields[random.randint(0, len(empty_fields) - 1)]

        return field
