import random
import time

from .Base import Player


class Validation(Player):

    def __init__(self, name="Validation", thinking_time=0.5):
        super().__init__(name)
        self.thinking_time = thinking_time

    def on_turn(self, board):
        time.sleep(self.thinking_time)

        values = [20, 0, 20, 0, 50, 0, 20, 0, 20]

        lines = [(0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6), (1, 4, 7), (2, 5, 8), (0, 4, 8), (2, 4, 6)]
        for line in lines:
            value = self.calc_value(board.fields, line)
            for index in line:
                values[index] += value

        value_sum = 0
        for index, field in enumerate(board.fields):
            if field == 0:
                value_sum += values[index]

        number = random.randint(0, value_sum)
        for index, field in enumerate(board.fields):
            if field == 0:
                number -= values[index]
                if number <= 0:
                    return index

    def calc_value(self, fields, line):
        own = 0
        other = 0
        for index in line:
            if fields[index] == self.number:
                own += 1
            elif fields[index] != 0:
                other += 1

        if own + other == 3:
            return 0
        elif own == 2 and other == 0:
            return 5000
        elif own == 0 and other == 2:
            return 1000
        elif own == 1 and other == 0:
            return 10
        elif own == 0 and other == 0:
            return 5
        elif own == 0 and other == 1:
            return 2
        else:
            return 1
