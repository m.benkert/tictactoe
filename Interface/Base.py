from Board import Board


class Interface:

    def on_creation(self, name_a: str, name_b: str):
        pass

    def visualize(self, board: Board):
        pass

    def on_game_end(self, winner: int):
        pass

    def output(self, txt: str):
        pass

    def interactive_turn(self, board: Board, number: int):
        pass
