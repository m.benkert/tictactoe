from .Base import Board, Interface


class Console(Interface):

    def __init__(self):
        self.players = None

    def on_creation(self, name_a, name_b):
        self.players = [name_a, name_b]

    def visualize(self, board):
        print(board)

    def on_game_end(self, winner):
        if winner == 0:
            print("Game ends tied")
        else:
            print("{} won the game!".format(self.players[winner - 1]))
            print("{} lost the game!".format(self.players[winner % 2]))

    def output(self, txt):
        print(txt)

    def interactive_turn(self, board, number):
        empty_fields = []
        colors = [" ", "x", "o"]
        n_board = Board(colors=None)
        for index, element in enumerate(board.fields):
            if element == 0:
                empty_fields.append(index)
                n_board.set_field(index + 1, index=index)
            else:
                n_board.set_field(colors[element], index=index)

        print("Select field:")
        print(n_board)

        field = 0
        while field - 1 not in empty_fields:
            print("Your Selection (you are {}):".format(colors[number]), end=" ")
            try:
                field = int(input())
            except ValueError:
                continue
        return field - 1
