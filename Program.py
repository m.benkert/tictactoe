import Addon
import Interface
import Player

from Game import Game


def main():
    # Addons
    upload = Addon.Upload()
    statistics = Addon.Statistics()

    # Interface
    console = Interface.Console()

    # Players
    player_random = Player.Random(thinking_time=0)
    player_validation = Player.Validation(thinking_time=0)
    player_ai = Player.ANN(thinking_time=0)
    player_reinforce = Player.Reinforce(thinking_time=0, episodes=1000)
    # player_reinforce.load_object_from_file()

    player_human = Player.Human(interface=console)

    # Game
    game = Game(player_validation, player_reinforce, console, statistics)
    for _ in range(1000):
        game.new_game()

    statistics.get_statistics()
    statistics.show_development()

    # Game
    game = Game(player_human, player_reinforce, console, statistics)
    for _ in range(5):
        game.new_game()

    statistics.get_statistics()
    statistics.show_development()


if __name__ == '__main__':
    main()
