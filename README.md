# TicTacToe

Implementation of the pen-and-paper-game "TicTacToe" with focus on Artificial Intelligence.

## Statistics (1000 games)

| Player A | Player B | Wins A | Wins B |
| --- | --- |:---:|:---:|
|Random | Validation | 40 | 853 |
|Random | Artificial Neural Network | 24 | 746 |
|Random | Reinforcement Learning | 30 | 774 |
|Validation | Artificial Neural Network | 134 | 310 |
|Validation | Reinforcement Learning | 4 | 417 |
|Artificial Neural Network | Reinforcement Learning | 1 | 0 |
