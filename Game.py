import numpy
import random
from Board import Board
from Addon.Base import Addon
from Interface.Base import Interface
from Player.Base import Player


class Game:

    def __init__(self, player_a: Player, player_b: Player, interface: Interface, *add_ons: Addon):
        self.game = Board()
        self.colors = [' ', 'x', 'o']

        player_a.set_number(1)
        player_b.set_number(2)
        self.players = [player_a, player_b]
        self.current = None

        self.interface = interface
        self.interface.on_creation(player_a.get_name(), player_b.get_name())

        self.add_ons = add_ons
        for add_on in self.add_ons:
            add_on.on_creation(player_a.get_name(), player_b.get_name(), self.interface)

    def new_game(self):
        self.on_game_start()

        while not self.game_is_over():
            self.on_turn()

        self.on_game_end()

    def on_game_start(self):
        self.game.clear_board()
        self.players[0].on_game_start()
        self.players[1].on_game_start()
        self.current = random.randint(1, 2)

        for add_on in self.add_ons:
            add_on.on_game_start(self.current)

    def on_turn(self):
        self.visualize()
        field = self.active_player().on_turn(self.game.copy())
        try:
            self.game.set_field(self.current, index=field)
        except ValueError:
            return
        self.current = self.current % 2 + 1
        for add_on in self.add_ons:
            add_on.on_turn(field)

    def on_game_end(self):
        self.visualize()

        winner = self.someone_has_won()
        self.interface.on_game_end(winner)
        self.players[0].on_game_end(winner)
        self.players[1].on_game_end(winner)

        for add_on in self.add_ons:
            add_on.on_game_end(winner)

    def active_player(self):
        return self.players[self.current - 1]

    def game_is_over(self):
        if numpy.prod(self.game.get_fields()) != 0:
            return True
        if self.someone_has_won() != 0:
            return True
        return False

    def someone_has_won(self):
        lines = self.game.get_rows()
        lines.extend(self.game.get_columns())
        lines.extend(self.game.get_diagonals())
        for line in lines:
            if line[0] == 0:
                continue
            if line[0] == line[1] == line[2]:
                return line[0]

        return 0

    def visualize(self):
        # self.interface.visualize(self.game.copy())
        pass

    def output(self, txt: str):
        self.interface.output(txt)
