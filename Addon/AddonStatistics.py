import numpy as np
import matplotlib.pyplot as plt

from .Base import Addon


class Statistics(Addon):
    def __init__(self):
        super().__init__()
        self.wins = None
        self.win_list = None
        self.total_games = None

    def on_creation(self, name_a, name_b, interface):
        super().on_creation(name_a, name_b, interface)
        self.wins = [0, 0]
        self.win_list = []
        self.total_games = 0

    def on_game_end(self, winner):
        if winner != 0:
            self.wins[winner - 1] += 1
        self.win_list.append(winner)
        self.total_games += 1

    def get_statistics(self):
        self.interface.output("Total Games: {}".format(self.total_games))
        for x in range(2):
            self.interface.output("{}: {:.2f}% winning rate".format(self.names[x],
                                                                    self.wins[x] / self.total_games * 100))

    def show_development(self):
        player_a = np.zeros(self.total_games)
        player_b = np.zeros(self.total_games)

        if self.win_list[0] == 1:
            player_a[0] = 1
        elif self.win_list[0] == 2:
            player_b[0] = 1

        for index in range(1, self.total_games):
            player_a[index] = player_a[index - 1]
            player_b[index] = player_b[index - 1]

            if self.win_list[index] == 1:
                player_a[index] += 1
            elif self.win_list[index] == 2:
                player_b[index] += 1

        plt.plot(np.linspace(0, self.total_games, self.total_games), player_a, label=self.names[0])
        plt.plot(np.linspace(0, self.total_games, self.total_games), player_b, label=self.names[1])
        plt.legend()
        plt.show()
