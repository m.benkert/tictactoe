import MySQLdb
from .Base import Addon
from Board import Board


class Upload(Addon):
    def __init__(self):
        super().__init__()
        self.starting_player = None
        self.turns_so_far = None

    def on_game_start(self, starting_player):
        self.starting_player = starting_player
        self.turns_so_far = []

    def on_turn(self, field):
        self.turns_so_far.append(field)

    def on_game_end(self, winner):
        board = Board()
        values = []
        weights = self.create_weight_list(winner)
        for index, field in enumerate(self.turns_so_far):
            fields = board.get_fields() + [field]
            values.append((fields, weights[index]))
            board.set_field(color=1, index=field)
            board.swap_numbers()
        self.update_data_set(values)

    def create_weight_list(self, winner: int):
        if winner == 0:
            weights = [[0, 1, 0] for _ in range(9)]
        elif winner == self.starting_player:
            weights = [[(x + 1) % 2, 0, x % 2] for x in range(9)]
        else:
            weights = [[x % 2, 0, (x + 1) % 2] for x in range(9)]
        return weights

    @staticmethod
    def update_data_set(values):
        print(values)
        db = MySQLdb.connect(host="localhost", user="root", password="", database="data_set")

        cursor = db.cursor()

        for value in values:
            fields, weight = value
            column_id = "".join([str(x) for x in fields])

            sql_select = "SELECT won, tied, lost from tictactoe where ID LIKE '{}'".format(column_id)
            cursor.execute(sql_select)
            if cursor.rowcount > 0:
                records = cursor.fetchall()
                weight = [weight[x] + records[0][x] for x in range(3)]
                sql = "UPDATE tictactoe SET field0 = %s, field1 = %s, field2 = %s, field3 = %s, field4 = %s, " \
                      "field5 = %s, field6 = %s, field7 = %s, field8 = %s, decision = %s, won = %s, tied = %s, " \
                      "lost = %s WHERE ID = %s "
            else:
                sql = "INSERT INTO tictactoe (field0, field1, field2, field3, field4, field5, field6, field7, field8, "\
                      "decision, won, tied, lost, ID) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) "
            val = tuple(fields) + tuple(weight) + tuple([column_id])
            cursor.execute(sql, val)

        db.commit()
