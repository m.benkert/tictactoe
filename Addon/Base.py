from Interface.Base import Interface


class Addon:
    def __init__(self):
        self.names = None
        self.interface = None

    def on_creation(self, name_a: str, name_b: str, interface: Interface):
        self.names = [name_a, name_b]
        self.interface = interface

    def on_game_start(self, starting_player: int):
        pass

    def on_turn(self, field: int):
        pass

    def on_game_end(self, winner: int):
        pass
