from Board import Board
from unittest import TestCase


class BoardTest(TestCase):

    def test_init(self):
        # given
        fields = list(range(9))
        # when
        board = Board()
        board2 = Board(fields)
        # then
        self.assertEqual([0 for _ in range(9)], board.fields)
        self.assertEqual(fields, board2.fields)

    def test_str(self):
        # given
        board = Board([0, 0, 0, 1, 1, 1, 2, 2, 2])
        # when
        s = str(board)
        # then
        print(s)
        self.assertEqual("   |     |   \n-  +  -  +  -\nx  |  x  |  x\n-  +  -  +  -\no  |  o  |  o\n", s)

    def test_copy(self):
        # given
        board = Board(list(range(9)))
        # when
        board2 = board.copy()
        # then
        self.assertEqual(board.fields, board2.fields)

        board2.fields = [0 for _ in range(9)]
        self.assertNotEqual(board.fields, board2.fields)

    def test_clear_board(self):
        # given
        board = Board(list(range(9)))
        # when
        board.clear_board()
        # then
        self.assertEqual([0 for _ in range(9)], board.fields)

    def test_swap_numbers(self):
        # given
        board = Board([0, 0, 0, 1, 1, 1, 2, 2, 2])
        # when
        board.swap_numbers()
        # then
        self.assertEqual([0, 0, 0, 2, 2, 2, 1, 1, 1], board.fields)

    def test_set_field(self):
        # given
        index = 5
        position = (2, 2)
        board = Board()
        # when
        board.set_field(1, index=index)
        board.set_field(2, *position)
        # then
        self.assertEqual(1, board.fields[index])
        self.assertEqual(2, board.fields[position[0] * 3 + position[1]])

    def test_get_rows(self):
        # given
        board = Board(list(range(9)))
        # when
        rows = board.get_rows()
        # then
        self.assertEqual([0, 1, 2], rows[0])
        self.assertEqual([3, 4, 5], rows[1])
        self.assertEqual([6, 7, 8], rows[2])

    def test_get_columns(self):
        # given
        board = Board(list(range(9)))
        # when
        columns = board.get_columns()
        # then
        self.assertEqual([0, 3, 6], columns[0])
        self.assertEqual([1, 4, 7], columns[1])
        self.assertEqual([2, 5, 8], columns[2])

    def test_get_diagonals(self):
        # given
        board = Board(list(range(9)))
        # when
        diagonals = board.get_diagonals()
        # then
        self.assertEqual([0, 4, 8], diagonals[0])
        self.assertEqual([2, 4, 6], diagonals[1])

    def test_get_fields(self):
        # given
        given_fields = list(range(9))
        board = Board(given_fields)
        # when
        fields = board.get_fields()
        # then
        self.assertEqual(given_fields, fields)
