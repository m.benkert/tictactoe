
class Board:

    def __init__(self, fields: list = None, colors=(' ', 'x', 'o')):
        if fields is None:
            fields = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.fields = fields
        self.colors = colors

    def __str__(self):
        board = ""
        for x in range(3):
            if x != 0:
                for y in range(3):
                    if y != 0:
                        board += '  +  '
                    board += '-'
                board += '\n'
            for y in range(3):
                if y != 0:
                    board += '  |  '
                if self.colors is None:
                    board += str(self.fields[3 * x + y])
                else:
                    board += self.colors[self.fields[3 * x + y]]
            board += '\n'
        return board

    def copy(self):
        new_board = Board(self.get_fields().copy())
        return new_board

    def clear_board(self):
        self.fields = [0, 0, 0, 0, 0, 0, 0, 0, 0]

    def swap_numbers(self):
        for index, number in enumerate(self.fields):
            if number == 1:
                self.fields[index] = 2
            elif number == 2:
                self.fields[index] = 1

    def set_field(self, color: int, row: int = None, column: int = None, index: int = None):
        if row is not None and column is not None:
            index = row * 3 + column
        if index is not None and 0 <= index <= 8 and self.fields[index] == 0:
            self.fields[index] = color
        else:
            raise ValueError

    def get_rows(self):
        return [self.fields[0:3], self.fields[3:6], self.fields[6:9]].copy()

    def get_columns(self):
        columns = []
        for x in range(3):
            column = []
            for y in range(3):
                column.append(self.fields[x + y * 3])
            columns.append(column)
        return columns.copy()

    def get_diagonals(self):
        diagonals = []
        lines = [(0, 4, 8), (2, 4, 6)]
        for line in lines:
            diagonals.append([self.fields[i] for i in line])
        return diagonals.copy()

    def get_fields(self):
        return self.fields.copy()
